#!/bin/bash

echo '[*] Installing hacking tools with ptf'

cd /pentest/ptf

./ptf <<EOF
use modules/exploitation/metasploit
run
use modules/av-bypass/shellter
run
use modules/av-bypass/backdoorfactory
run
use modules/exploitation/beef
run
use modules/exploitation/exploit-db
run
use modules/exploitation/findsploit
run
use modules/exploitation/kingphisher
run
use modules/exploitation/responder
run
use modules/exploitation/routersploit
run
use modules/exploitation/setoolkit
run
use modules/exploitation/sqlmap
run
use modules/exploitation/wpsploit
run
use modules/intelligence-gathering/dnsrecon
run
use modules/intelligence-gathering/enum4linux
run
use modules/intelligence-gathering/recon-ng
run
use modules/intelligence-gathering/windows-exploit-suggester
run
use modules/password-recovery/hashcat
run
use modules/password-recovery/hashcat-utils
run
use modules/password-recovery/seclist
run
use modules/pivoting/pivoter
run
use modules/pivoting/dnscat2
run
use modules/post-exploitation/crackmapexec
run
use modules/post-exploitation/empire
run
use modules/post-exploitation/unicorn
run
use modules/post-exploitation/trevorc2
run
use modules/vulnerability-analysis/hydra
run
use modules/vulnerability-analysis/nikto
run
use modules/vulnerability-analysis/nmap
run
use modules/vulnerability-analysis/office365userenum
run
use modules/vulnerability-analysis/sslscan
run
use modules/vulnerability-analysis/wpscan
run
use modules/webshells/weevely
run
use modules/windows-tools/install_update_all
run
use modules/wireless/aircracking
run
use modules/wireless/kismet
run
EOF
