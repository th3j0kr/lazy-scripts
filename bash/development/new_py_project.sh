#!/bin/bash

#$1 is path of prject

P_NAME=$1

if [ $# == 1 ]; then

	if [ ! -d "$P_NAME" ]; then
		P_PATH="/root/sbin/$P_NAME"
		echo "[-] Making project folder at $P_PATH"
		mkdir $P_PATH
	else
		echo "[!] A project already exists with that name!"
		echo "[!] Choose a different name for your project!"
	fi

	cd $P_PATH

	echo "[-] Creating directory structure"
	mkdir bin
	mkdir -p data/logs/info
	mkdir data/logs/error
	mkdir data/conf
	mkdir data/notes
	mkdir lib
	mkdir modules

	echo "[-] Getting the jacob standard library"
	git clone https://gitlab.purpleteamsec.com/th3J0kr/th3j0kr-pylib.git ./lib/

	echo "[*] All done, now just sync $P_PATH from remote with vscode/rsync!"
else
	echo "[!] Don't forget project path!"
	echo "[!] Usage ./new_py_project.sh <path to project>"
fi
