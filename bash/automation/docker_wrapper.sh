#!/bin/bash

if [ $# -lt 2 ]; then
    echo '[!] ERROR: Please provide at least author and image name'
    echo '[*] USAGE: ./docker_wrapper.sh -a <author> -i <image> [-v <volume mapping>] [-c <permissions>]'

else
    #SET CONTANSTANTS
    AUTHOR=""
    IMAGE=""
    VOLUMES=()
    PERMS=()
    # Process arguments
    while getopts "a:i:v:c:" opt; do
        case ${opt} in
            i )
                IMAGE=$OPTARG
                ;;
            a )
                AUTHOR=$OPTARG
                ;;
            v )
                VOLUMES+=("$OPTARG")
                ;;
            c )
                PERMS+=("$OPTARG")
                ;;
            \? )
                echo "Invalid option: $OPTARG" 1>&2
                ;;
        esac
    done
    echo "[-] Author: $AUTHOR"
    echo "[-] Image: $IMAGE"
    echo "[-] Mapped Volumes: ${VOLUMES[*]}"
    echo "[-] Special Permissions: ${PERMS[*]}"
    
    # Remove any stop and remove container if it is running
    docker rm -f $IMAGE 1>&2
    # Make dir for logs
    if [ ! -d "/var/log/docker_apps/$IMAGE" ]; then
        mkdir "/var/log/docker_apps/$IMAGE"
    fi
    # Build run command
    CMD="docker run -d"
    # Add permissions
    for perm in "${PERMS[@]}"; do
        CMD="$CMD --cap-add $perm"
    done
    CMD="$CMD $pString"
    # Add volumes
    for vol in "${VOLUMES[@]}"; do
        CMD="$CMD -v $vol"
    done
    # Add basic stuff
    CMD="$CMD -v /var/log/docker_apps/$IMAGE:/app/data/logs --name $IMAGE sectools.luxgroup.net:4567/$AUTHOR/$IMAGE"
    echo "[-] Running Command: $CMD"
    eval $CMD

    #docker run -d --cap-add SYS_ADMIN --cap-add DAC_READ_SEARCH -v /var/log/docker_apps/$IMAGE:/app/data/logs -v /root/sbin/plm-processor/data/var:/app/data/var --name $IMAGE sectools.luxgroup.net:4567/$AUTHOR/$IMAGE

fi