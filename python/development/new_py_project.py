import os, sys, argparse, shutil, subprocess, stat
class NewPyProject:
    "Script to create a new bare-bones python project"
    def __init__(self):
        #-=-=-=-=-=-=-=-= Edit variables here -=-=-=-=-=-=-=-=
        self.TEMPLATE_URL = 'https://gitlab.purpleteamsec.com/th3J0kr/example-python-project.git'
        self.FLASK_TEMPLATE_URL = 'https://gitlab.purpleteamsec.com/th3J0kr/flask-example-app.git'
        self.API_TEMPLATE_URL = 'https://gitlab.purpleteamsec.com/th3J0kr/restful-api-example.git'
        self.LIB_URL = 'https://gitlab.purpleteamsec.com/th3J0kr/th3j0kr-pylib.git'
        #-=-=-=-=-=-=-=-= End edit variables -=-=-=-=-=-=-=-=
        self.parser = argparse.ArgumentParser()
        self.args = self.parseArgs()
        self.PATH = self.args.path

    def parseArgs(self):
        self.parser.add_argument("path", help='The full path to make the project')
        self.parser.add_argument("--flask", action='store_true', help='Create a flask app')
        self.parser.add_argument("--api", action='store_true', help='Create a flask RESTful api app')
        args = self.parser.parse_args()
        return args
    
    def setupDirs(self, path):
        "Setup project directory"
        if not os.path.isdir(path):
            print(f'[-] Creating project directory at {path}')
            os.mkdir(path)
        else:
            print(f'[-] Project directory already exists..')

    def getTemplate(self):
        "Get the bare bones template from gitlab"
        print(f'[+] Getting the python project template')
        if self.args.flask:
            # Clone the flask repo
            subprocess.call(['git', 'clone', self.FLASK_TEMPLATE_URL, self.PATH])
        elif self.args.api:
            # Clone the flask RESTful api repo
            subprocess.call(['git', 'clone', self.API_TEMPLATE_URL, self.PATH])
        else:
            # Clone the repo
            subprocess.call(['git', 'clone', self.TEMPLATE_URL, self.PATH])
        # Remove git folder
        #gitPath = os.path.join(self.PATH, '.git')
        #shutil.rmtree(os.path.join(self.PATH, '.git'))

    def getLib(self):
        "Get the bare bones template from gitlab"
        print(f'[+] Getting the python library')
        # Clone the repo
        self.libPath = os.path.join(self.PATH, 'lib')
        subprocess.call(['git', 'clone', self.LIB_URL, self.libPath])

    def main(self):
        "The main controller function"
        print(f'[+] Creating new python project for {os.path.basename(self.PATH).split(".")[0]}')
        self.setupDirs(self.PATH)
        self.getTemplate()
        self.getLib()
        print(f'[+] Done! Start coding at {os.path.basename(self.PATH).split(".")[0]}')
        print('[!] Don\'t forget to remove the git folders:') 
        print(f'    - {os.path.join(os.path.basename(self.PATH).split(".")[0], ".git")}')
        print(f'    - {os.path.join(os.path.basename(self.PATH).split(".")[0], "lib", ".git")}')

if __name__ == '__main__':
    npp = NewPyProject()
    npp.main()
