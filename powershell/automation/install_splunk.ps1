$SHARE_PATH = "\\range-dc\SYSVOL\splunk"
$INPUTS_PATH = Join-Path -Path $SHARE_PATH -ChildPath "inputs.conf"
$OUTPUTS_PATH = Join-Path -Path $SHARE_PATH -ChildPath "outputs.conf"
$SPLUNK_MSI_PATH = Join-Path -Path $SHARE_PATH -ChildPath "splunkforwarder-7.2.4-8a94541dcfac-x64-release.msi"
$SPLUNK_PATH = "C:\Program Files\SplunkUniversalForwarder"
$SPLUNK_BIN = Join-Path -Path $SPLUNK_PATH -ChildPath "bin\splunk.exe"
$SPLUNK_LOCAL_INPUTS = Join-Path -Path $SPLUNK_PATH -ChildPath "etc\system\local\inputs.conf"
$SPLUNK_LOCAL_OUTPUTS = Join-Path -Path $SPLUNK_PATH -ChildPath "etc\system\local\outputs.conf"

#Install Splunk and wait for install to finish
msiexec.exe /i $SPLUNK_MSI_PATH LOGON_USERNAME="RANGE\splunk" LOGON_PASSWORD="P@$$w0rd" LAUNCHSPLUNK=0 AGREETOLICENSE=Yes /quiet
Start-Sleep -Seconds 60

#Set SPLUNK_HOME environment variable
[Environment]::SetEnvironmentVariable("SPLUNK_HOME", $SPLUNK_PATH, "User")
[Environment]::SetEnvironmentVariable("SPLUNK_HOME", $SPLUNK_PATH, "Machine")

# Copy config
#Copy-Item -Path $INPUTS_PATH -Destination $SPLUNK_LOCAL_INPUTS -Force
#Copy-Item -Path $OUTPUTS_PATH -Destination $SPLUNK_LOCAL_OUTPUTS -Force

# Start/restart splunk
Start-Process powershell {& 'C:\Program Files\SplunkUniversalForwarder\bin\splunk.exe' start; Read-Host}