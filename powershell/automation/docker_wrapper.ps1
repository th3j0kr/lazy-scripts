# Define parameters
param (
    [Parameter(Mandatory=$true)][string]$author,
    [Parameter(Mandatory=$true)][string]$image,
    [string[]]$volumes,
    [string[]]$perms,
    [string[]]$ports,
    [switch]$persistance = $false,
    [switch]$interactive = $false,
    [switch]$dev = $false
)
Write-output "[-] Author: $author"
Write-output "[-] Image: $image"

If (!($volumes -eq $null)) {
    "[-] Volume: $volumes" 
}
If (!($perms -eq $null)) {
    "[-] Permissions: $perms"
}
If (!($ports -eq $null)) {
    "[-] Ports: $ports"
}
docker rm -f $image

$DOCKER_LOGDIR = 'C:\Users\c11900\Documents\Engineering\prog\docker_logs'
if (!(Test-Path -Path $DOCKER_LOGDIR)){
    New-Item -ItemType directory -Path "$DOCKER_LOGDIR\$image"
}

# Build command string
$CMD_STRING = "docker run "
if ($interactive -eq $true) {
    Set-Variable -Name CMD_STRING -Value "$CMD_STRING -it"
}
if ($dev -eq $true) {
    Set-Variable -Name CMD_STRING -Value "$CMD_STRING -v ${DOCKER_LOGDIR}:/app/data/logs"
}
If (!($volumes -eq $null)) {
    foreach ($vol in $volumes) {
        Set-Variable -Name CMD_STRING -Value "$CMD_STRING -v $vol"    
    }
}
If (!($perms -eq $null)) {
    foreach ($perm in $perms) {
        Set-Variable -Name CMD_STRING -Value "$CMD_STRING --cap-add $perm"    
    }
}
If (!($ports -eq $null)) {
    foreach ($port in $port) {
        Set-Variable -Name CMD_STRING -Value "$CMD_STRING -p $port"    
    }
}
if ($persistance -eq $true) {
    Set-Variable -Name CMD_STRING -Value "$CMD_STRING --mount src=ubuntu-root,dst=/root"
}

Set-Variable -Name CMD_STRING -Value "$CMD_STRING sectools.luxgroup.net:4567/$author/$image"

Write-output "[*] Running command: $CMD_STRING"

Invoke-Expression $CMD_STRING